import bpy


class _Preferences():
    @property
    def prefs(self):
        return bpy.context.preferences.addons[__package__.split('.')[0]].preferences

    @property
    def separator(self):
        return self.prefs.separator

    @property
    def type_flags(self):
        return {
            'DEF': self.prefs.deform_flag,
            'TRGT': self.prefs.target_flag,
            'MCH': self.prefs.mechanism_flag,
            'CTRL': self.prefs.control_flag,
            'NONE': '',
        }

    @property
    def side_flags(self):
        return {
            'MID': self.prefs.mid_flag,
            'RIGHT': self.prefs.right_flag,
            'LEFT': self.prefs.left_flag,
            'NONE': '',
        }

    @property
    def warning_behaviour(self):
        return self.prefs.warning_behaviour


preferences = _Preferences()
