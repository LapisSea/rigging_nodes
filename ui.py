from .sockets.socket import draw_times, get_value_times
from .runtime import cache_nodetree_times
import functools
import bpy


class BBN_UL_created_objects(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        node = data.get_node_from_path(data.deserialize_path(item.orig_node_path))
        row = layout.row()
        if not node:
            row.alert = True
        row.operator('bbn.focus_path_node', text='', icon='RESTRICT_SELECT_OFF').path = item.orig_node_path
        row.operator('bbn.select_register_object', text='', icon='VIS_SEL_11').index = index
        if item.value:
            row.label(text=item.value.name)
        else:
            row.label(text='(DELETED OBJECT)')
        row.operator('bbn.remove_created_object', text='', icon='UNLINKED').index = index

    def invoke(self, context, event):
        pass


class BBN_PT_tree_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_tree_panel"
    bl_label = "Node Tree"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Rigging Nodes"

    @classmethod
    def poll(cls, context):
        # return True
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        tree = context.space_data.node_tree
        if tree:
            layout = self.layout

            layout.label(text=tree.name, icon='OUTLINER_OB_ARMATURE')

            col = layout.column(align=True)

            row = col.split(factor=0.1, align=True)
            row.scale_y = 2
            row.prop(tree, 'auto_update', text='', toggle=True, icon_only=True, icon='FILE_REFRESH')
            row.operator("bbn.generate_rig", text="Execute (e)", icon='RESTRICT_VIEW_OFF').node_name = ''

            col = layout.column(align=False)
            col.operator("bbn.update_nodes", text="Update Nodes", icon='FILE_REFRESH')
            row = col.row(align=True)
            row.operator('bbn.auto_frame')
            row.operator('bbn.auto_frame_delete', text='', icon='X')

            col.prop(bpy.context.preferences.addons[__package__.split('.')[0]].preferences, 'warning_behaviour', text='Warnings')


class BBN_PT_tree_objects_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_tree_objects_panel"
    bl_label = "Created Objects"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Rigging Nodes"

    @classmethod
    def poll(cls, context):
        # return True
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        tree = context.space_data.node_tree
        if tree:
            self.layout.template_list("BBN_UL_created_objects", "", tree, "objects", tree, "object_index", rows=4)


class BBN_PT_tree_variables_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_tree_variables_panel"
    bl_label = "Tree Variables"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Rigging Nodes"

    @classmethod
    def poll(cls, context):
        # return True
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        tree = context.space_data.edit_tree
        if not tree:
            return

        box = self.layout.box()

        nodes = [x for x in tree.nodes if x.bl_idname == 'bbn_set_variable']
        if nodes:
            for x in nodes:
                row = box.row()
                row.operator('bbn.focus_node', text='', icon='RESTRICT_SELECT_OFF').node = x.name
                row.prop(x, 'variable_name', text='')
                row.operator('bbn.select_referencing_nodes', text='', icon='TRACKING_REFINE_FORWARDS').node = x.name
        else:
            box.label(text='Create reusable variables with the "Get/Set Tree Variable" nodes')


class BBN_PT_tree_times_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_tree_times_panel"
    bl_label = "Execution Times"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Rigging Nodes"

    @classmethod
    def poll(cls, context):
        # return True
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        tree = context.space_data.node_tree
        if tree:
            layout = self.layout

            row = layout.row()
            row.scale_y = 1.5
            row.prop(tree, 'show_times', text='Show Node Execution Times', toggle=True)

            for key, item in cache_nodetree_times.items():
                icon = 'NONE'
                if item < 0.01:
                    icon = 'KEYTYPE_JITTER_VEC'
                elif item < 0.1:
                    icon = 'KEYTYPE_MOVING_HOLD_VEC'
                else:
                    icon = 'KEYTYPE_EXTREME_VEC'
                layout.label(icon=icon, text=f"{key}: {item:.5f}")

            edit_tree = context.space_data.edit_tree
            if edit_tree in draw_times:
                layout.label(text="socket draw: {:.5f}".format(functools.reduce(lambda a, b: a + b, draw_times[edit_tree].values())))
            if edit_tree in get_value_times:
                layout.label(text="socket get value: {:.5f}".format(functools.reduce(lambda a, b: a + b, get_value_times[edit_tree].values())))


class BBN_PT_utils_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_utils_panel"
    bl_label = "Rigging Nodes"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Item"

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def draw(self, context):
        layout = self.layout

        layout.operator('bbn.edit_bone_shape')


'''Replace the node editor header with an override to place our own custom "Create node tree" operator.'''
original_header_draw = bpy.types.NODE_HT_header.draw


def node_header_draw_override(self_real, context):
    from bl_ui.space_node import NODE_MT_editor_menus
    if context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree' and not context.space_data.node_tree:
        self_real.layout.template_header()
        NODE_MT_editor_menus.draw_collapsible(context, self_real.layout)
        self_real.layout.separator_spacer()
        self_real.layout.template_ID(context.space_data, "node_tree", new="bbn.new")
        self_real.layout.separator_spacer()
    else:
        original_header_draw(self_real, context)


classes = [
    BBN_UL_created_objects,
    BBN_PT_tree_panel,
    BBN_PT_tree_objects_panel,
    BBN_PT_tree_variables_panel,
    BBN_PT_tree_times_panel,
    BBN_PT_utils_panel,
]


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)

    bpy.types.NODE_HT_header.draw = node_header_draw_override


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)

    bpy.types.NODE_HT_header.draw = original_header_draw
