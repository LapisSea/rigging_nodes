import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_sort_array(Node, BBN_node):
    bl_idname = 'bbn_node_sort_array'
    bl_label = "Sort"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Reverse': {'type': 'BBN_bool_socket'},
    }

    _opt_input_sockets = {
        'Expression (x)': {'type': 'BBN_string_socket', 'default_value': 'x'}
    }

    def process(self, context, id, path):
        array = self.get_input_value('Array').copy()
        reverse = self.get_input_value('Reverse')
        expression = self.get_input_value('Expression (x)')

        if expression:
            def exp(x): return eval(expression)
            array.sort(key=exp, reverse=reverse)
        else:
            array.sort(reverse=reverse)

        self.set_output_value('Array', array)

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})

        self.remove_incorrect_links()
