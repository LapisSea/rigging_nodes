from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_repeat_array(Node, BBN_node):
    bl_idname = 'bbn_node_repeat_array'
    bl_label = "Repeat"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Times': {'type': 'BBN_int_socket'}
    }

    def process(self, context, id, path):
        array = self.get_input_value('Array', required=True)
        times = self.get_input_value('Times', required=True)

        if not array:
            raise ValueError('Array is empty')

        if self.outputs:
            self.set_output_value('Array', array * times)

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})

        self.remove_incorrect_links()
