from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


class BBN_node_set_armature_layers(Node, BBN_node):
    bl_idname = 'bbn_set_armature_layers_node'
    bl_label = "Set Armature Layers"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    bl_width_default = 300

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'}
    }

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'}
    }

    layers_0: bpy.props.BoolVectorProperty(size=8, default=[True, False, False, False, False, False, False, False], update=BBN_tree.value_updated)
    layers_1: bpy.props.BoolVectorProperty(size=8, update=BBN_tree.value_updated)
    layers_2: bpy.props.BoolVectorProperty(size=8, update=BBN_tree.value_updated)
    layers_3: bpy.props.BoolVectorProperty(size=8, update=BBN_tree.value_updated)

    layers_protected: bpy.props.BoolVectorProperty(size=32)

    def draw_buttons(self, context, layout):
        col = layout.column(align=True)
        row = col.row(align=True)
        row.prop(self, "layers_0", text="", icon='RADIOBUT_OFF')
        row.separator()
        row.prop(self, "layers_1", text="", icon='RADIOBUT_OFF')
        row = col.row(align=True)
        row.prop(self, "layers_2", text="", icon='RADIOBUT_OFF')
        row.separator()
        row.prop(self, "layers_3", text="", icon='RADIOBUT_OFF')

        super().draw_buttons(context, layout)

    def process(self, context, id, path):
        obj = self.get_input_value("Object")
        armature = obj.data

        armature.layers = [x for x in self.layers_0] + [x for x in self.layers_1] + [x for x in self.layers_2] + [x for x in self.layers_3]

        self.set_output_value(0, obj)
