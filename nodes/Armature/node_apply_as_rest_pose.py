import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_apply_as_rest_pose(Node, BBN_node):
    bl_idname = 'bbn_node_apply_as_rest_pose'
    bl_label = "Apply as Rest Pose"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('ALL', 'All', 'All'),
        ('SINGLE', 'Single', 'Single'),
        ('MULTIPLE', 'Multiple', 'Multiple'),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bones': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Bones': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bones': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Bones': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bones = self.get_input_value('Bones', force_list=True)

        for bone in bones:
            armature.data.BBN_info.add_bone_tracking(bone, self, path)

        override = context.copy()
        override['selected_objects'] = override['selected_objects'] = [armature]
        override['object'] = override['active_object'] = armature
        if bones:
            override['selected_pose_bones'] = [armature.pose.bones[bone_name] for bone_name in bones]
        elif not self.current_behaviour == 'ALL':
            raise ValueError('No bones to apply rest pose found')

        bpy.ops.pose.armature_apply(override, selected=not self.current_behaviour == 'ALL')

        self.set_output_value('Armature', armature)

        if self.has_output('Bones'):
            if self.current_behaviour == 'SINGLE':
                self.set_output_value('Bones', bones[0])
            else:
                self.set_output_value('Bones', bones)
