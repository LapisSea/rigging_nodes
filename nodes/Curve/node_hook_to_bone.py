import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_hoot_to_bone(Node, BBN_node):
    bl_idname = 'bbn_hoot_to_bone_node'
    bl_label = "Hook to bone"

    bl_width_default = 250

    input_sockets = {
        'Curve': {'type': 'BBN_object_socket'},
        'Armature': {'type': 'BBN_object_ref_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only hooks one bone to the desired curve index', '', 0),
        ('MULTIPLE', 'Multiple', 'Hooks all bones in the array in order, to all curve points', '', 1),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
                'Index': {'type': 'BBN_int_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket', 'array_socket_type': 'BBN_string_socket'},
            }
        },
    }

    output_sockets = {
        'Curve': {'type': 'BBN_object_socket'},
    }

    _opt_input_sockets = {
        'hook_first_handle': {
            'display_name': 'Hook First Handle',
            'description': 'If true, the first handle of the given curve at the given index will be hooked',
            'type': 'BBN_bool_socket'
        },
        'hook_curve_point': {
            'display_name': 'Hook Curve Point',
            'description': 'If true, the point of the curve at the given index will be hooked',
            'type': 'BBN_bool_socket'
        },
        'hook_last_handle': {
            'display_name': 'Hook Last Handle',
            'description': 'If true, the last handle of the given curve at the given index will be hooked',
            'type': 'BBN_bool_socket'
        },
    }

    input_to_focus = 'Curve'
    focus_mode = {'EDIT'}

    def process(self, context, id, path):
        curve = self.get_input_value('Curve', required=True)
        armature = self.get_input_value('Armature', required=True)
        bones = self.get_input_value('Bone')

        hook_first = self.get_input_value('hook_first_handle', default=True)
        hook_mid = self.get_input_value('hook_curve_point', default=True)
        hook_last = self.get_input_value('hook_last_handle', default=True)
        hook_bool_array = [hook_first, hook_mid, hook_last]

        def create_hook(bone, index):
            hook_mod = curve.modifiers.new("hook", "HOOK")
            hook_mod.object = armature
            hook_mod.subtarget = bone

            hook_array = [index * 3 + i for i, x in enumerate(hook_bool_array) if x]

            hook_mod.vertex_indices_set(hook_array)

        if self.current_behaviour == 'SINGLE':
            index = self.get_input_value('Index')
            create_hook(bones, index)
        elif self.current_behaviour == 'MULTIPLE':
            for i, bone in enumerate(bones):
                create_hook(bone, i)

        self.outputs['Curve'].set_value(curve)

        bpy.ops.object.mode_set(mode='OBJECT')
