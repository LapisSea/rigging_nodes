import bpy
import mathutils
import string
from bpy.types import Node, Operator

from ..node_base import BBN_node
from ...sockets import sockets as sockets_list
from ...runtime import runtime_info

possible_sockets = {
    x.bl_label: x.bl_idname for x in sockets_list if x.bl_idname not in {'BBN_add_array_socket'}
}


class BBN_node_expression(Node, BBN_node):
    bl_idname = 'bbn_expression'
    bl_label = "Expression"
    bl_icon = 'SCRIPTPLUGINS'

    bl_width_default = 250

    description = '''Available modules:
bpy
mathutils'''

    input_sockets = {
        'Expression': {'type': 'BBN_string_socket'},
        '__virtual__': {'type': 'BBN_add_array_socket'}
    }

    output_sockets = {
        'output': {'type': 'BBN_add_array_socket'}
    }

    def get_unique_name(self, sockets):
        i = 1
        names = [x.name for x in sockets]
        ascii_letters = (x for x in string.ascii_lowercase)
        name = next(ascii_letters)
        while name in names:
            name = next(ascii_letters)
        return name

    @ property
    def opt_output_sockets(self):
        if not self.outputs or len(self.outputs) == 1 and self.outputs[0].bl_idname == 'BBN_add_array_socket':
            return {name: {'override_name': 'output', 'type': sock_type} for name, sock_type in possible_sockets.items()}
        return {}

    @ property
    def opt_input_sockets(self):
        ans = {name: {'override_name': self.get_unique_name(self.inputs), 'type': sock_type, 'editable_name': True, 'editable_name_node': True} for name, sock_type in possible_sockets.items()}
        return ans

    def execute_code(self):
        orig_globals = {'bpy': bpy, 'mathutils': mathutils}
        code_globals = {x.name: x.get_real_value() for x in self.inputs[:] if x.name != '__virtual__'}
        code_globals.update(orig_globals)
        code_locals = dict()
        code = compile(self.get_input_value('Expression'), '__expression_node__', 'eval')

        return eval(code, code_globals, code_locals)

    def process(self, context, id, path):
        if self.outputs:
            self.outputs[0].set_value(self.execute_code())

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs.get('__virtual__')
        if input:
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                try:
                    socket_to_add, socket_info = next((x, y) for x, y in self.opt_input_sockets.items() if y['type'] == incoming_type)
                    socket_info['is_deletable'] = True
                    new_socket = self.change_socket(self.inputs, socket_to_add, socket_info)
                    self.id_data.relink_socket(input, new_socket)

                except StopIteration:
                    pass

        self.remove_incorrect_links()

    def update_from_other(self, virtual_socket):
        output = self.outputs.get('output')
        if output and output == virtual_socket:
            connected_socket_0 = output.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                try:
                    socket_to_add, socket_info = next((x, y) for x, y in self.opt_output_sockets.items() if y['type'] == incoming_type)
                    socket_info['is_deletable'] = True
                    new_socket = self.change_socket(self.outputs, socket_to_add, socket_info)
                    self.id_data.relink_socket(output, new_socket)

                except StopIteration:
                    pass
