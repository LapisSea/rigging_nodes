import bpy
import string

from ..node_base import BBN_node
from ...sockets import sockets as sockets_list

possible_sockets = {
    x.bl_label: x.bl_idname for x in sockets_list if x.bl_idname not in {'BBN_add_array_socket'}
}


class BBN_OT_add_script_line(bpy.types.Operator):
    bl_idname = "bbn.add_script_line"
    bl_label = "Add Script Lines"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        node.lines.add()

        return {'FINISHED'}


class BBN_script_line(bpy.types.PropertyGroup):
    line: bpy.props.StringProperty()


class BBN_node_multi_line_script(bpy.types.Node, BBN_node):
    bl_idname = 'bbn_multi_line_script'
    bl_label = "Multi Line Script"
    bl_icon = 'SCRIPTPLUGINS'

    bl_width_default = 250

    input_sockets = {
        '__virtual__': {'type': 'BBN_add_array_socket'}
    }

    output_sockets = {
        '__virtual__': {'type': 'BBN_add_array_socket'}
    }

    lines: bpy.props.CollectionProperty(type=BBN_script_line)

    dependent_classes = [BBN_script_line, BBN_OT_add_script_line]

    def get_unique_name(self, sockets):
        names = [x.name for x in sockets]
        ascii_letters = (x for x in string.ascii_lowercase)
        name = next(ascii_letters)
        while name in names:
            name = next(ascii_letters)
        return name

    @ property
    def opt_output_sockets(self):
        ans = {name: {'override_name': self.get_unique_name(self.outputs), 'type': sock_type, 'editable_name': True, 'editable_name_node': True} for name, sock_type in possible_sockets.items()}
        return ans

    @ property
    def opt_input_sockets(self):
        ans = {name: {'override_name': self.get_unique_name(self.inputs), 'type': sock_type, 'editable_name': True, 'editable_name_node': True} for name, sock_type in possible_sockets.items()}
        return ans

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        col = layout.column(align=True)
        #col.scale_y = 0.9
        for x in self.lines:
            col.prop(x, "line", text='')
        row2.operator('bbn.add_script_line', icon='ADD', text='').node = self.name

    def execute_code(self):
        code_globals = {x.name: x.get_real_value() for x in self.inputs[:] if x.name != '__virtual__'}
        code_locals = dict()
        code_string = ''.join(f'{x.line}\n' for x in self.lines)
        code = compile(code_string, self.name, 'exec')
        exec(code, code_globals, code_locals)

        # print(code_globals.keys())
        # print(code_locals.keys())

        return code_globals, code_locals

    def process(self, context, id, path):
        code_globals, code_locals = self.execute_code()

        for x in self.outputs:
            if x.name in code_locals:
                x.set_value(code_locals[x.name])

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        input = self.inputs.get('__virtual__')
        if input:
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                try:
                    socket_to_add, socket_info = next((x, y) for x, y in self.opt_input_sockets.items() if y['type'] == incoming_type)
                    socket_info['is_deletable'] = True
                    new_socket = self.change_socket(self.inputs, socket_to_add, socket_info)
                    self.id_data.relink_socket(input, new_socket)

                except StopIteration:
                    pass

        self.remove_incorrect_links()

    def update_from_other(self, virtual_socket):
        output = self.outputs.get('__virtual__')
        if output and output == virtual_socket:
            connected_socket_0 = output.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                try:
                    socket_to_add, socket_info = next((x, y) for x, y in self.opt_output_sockets.items() if y['type'] == incoming_type)
                    socket_info['is_deletable'] = True
                    new_socket = self.change_socket(self.outputs, socket_to_add, socket_info)
                    self.id_data.relink_socket(output, new_socket)

                except StopIteration:
                    pass
