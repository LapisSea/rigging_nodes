import bpy
import math
from bpy.types import Node
import operator

from ..node_base import BBN_node

import mathutils
from collections import OrderedDict

functions_map = OrderedDict(
    identity=lambda: mathutils.Matrix.Identity(4),
    translation=lambda a: mathutils.Matrix.Translation(a).to_4x4(),
    rotation=lambda angle, axis: mathutils.Matrix.Rotation(angle, 4, axis),
    scale=lambda *args: mathutils.Matrix.Scale(args[0], 4, *args[1:]),
    diagonal=lambda a: mathutils.Matrix.Diagonal(a).to_4x4(),
    ortho_projection=lambda axis: mathutils.Matrix.OrthoProjection(axis, 4),
    shear=lambda plane, factor1, factor2: mathutils.Matrix.Shear(plane, 4, (factor1, factor2)),
    compose=lambda loc, rot, scale: mathutils.Matrix.Translation(loc).to_4x4() @ rot.to_matrix().to_4x4() @ mathutils.Matrix.Scale(1, 4, scale)
)


functions_enum = sorted([(x, x.replace('_', ' ').title(), x.replace('_', ' ').title(), 'NONE', i) for i, x in enumerate(list(functions_map.keys()))], key=lambda x: x[1])


class BBN_node_matrix_create_math(Node, BBN_node):
    bl_idname = 'bbn_node_matrix_create_math'
    bl_label = "Create Matrix"
    bl_icon = 'XRAY'

    bl_width_default = 200

    behaviour_enum = functions_enum

    behaviour_opt_sockets = {
        'scale': {
            'INPUTS': OrderedDict(
                Axis={'type': 'BBN_vector_socket'},
            ),
        }
    }

    behaviour_sockets = {
        'diagonal': {
            'INPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'identity': {
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'ortho_projection': {
            'INPUTS': OrderedDict(
                Axis={'type': 'BBN_enum_socket', 'items': ['XY', 'XZ', 'YZ']},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'rotation': {
            'INPUTS': OrderedDict(
                Angle={'type': 'BBN_float_socket'},
                Axis={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'scale': {
            'INPUTS': OrderedDict(
                Factor={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'shear': {
            'INPUTS': OrderedDict(
                Plane={'type': 'BBN_enum_socket', 'items': ['XY', 'XZ', 'YZ']},
                Factor_A={'type': 'BBN_float_socket'},
                Factor_B={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'translation': {
            'INPUTS': OrderedDict(
                Translation={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'compose': {
            'INPUTS': OrderedDict(
                Translation={'type': 'BBN_vector_socket'},
                Rotation={'type': 'BBN_quaternion_socket'},
                Scale={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        }
    }

    def draw_label(self):
        return f"Create Matrix: {self.current_behaviour.replace('_', ' ').title()}"

    def process(self, context, id, path):
        args = [self.get_input_value(i) for i in range(0, len(self.inputs))]

        func = functions_map[self.current_behaviour]

        if callable(func):
            result = func(*args)
        else:
            result = getattr(args[0], self.current_behaviour)

        if len(self.outputs) > 1:
            for i, x in enumerate(result):
                self.outputs[i].set_value(x)
        else:
            if result is None:
                raise ValueError('The provided arguments do not meet the requirements')
            self.outputs[0].set_value(result)
