import bpy
from bpy.types import Node
import operator

from ..node_base import BBN_node
from ...node_tree import BBN_tree
from collections import OrderedDict
import math

import mathutils

socket_names = {
    'BBN_float_socket': 'Float',
    'BBN_int_socket': 'Int',
    'BBN_vector_socket': 'Vector',
    'BBN_matrix_socket': 'Matrix',
    'BBN_quaternion_socket': 'Quaternion',
}

def operator_min(a,b):
    return min(a,b)

def operator_max(a,b):
    return max(a,b)

def rotate_matrix(matrix, other):
    matrix = matrix.to_3x3()

    matrix.rotate(other)
    return matrix.to_4x4()


math_functions = {
    'BBN_float_socket': OrderedDict(
        add=operator.add,
        subtract=operator.sub,
        multiply=operator.mul,
        division=operator.truediv,
        floor_division=operator.truediv,
        power=operator.pow,
        deg_to_rad=math.radians,
        rad_to_deg=math.degrees,
        ceil=math.ceil,
        absolute=math.fabs,
        floor=math.floor,
        modulo=math.fmod,
        truncate=math.trunc,
        lerp=lambda a, b, f: a + f * (b - a),
        acos=math.acos,
        asin=math.asin,
        atan=math.atan,
        atan2=math.atan2,
        cos=math.cos,
        sin=math.sin,
        tan=math.tan,
        min=operator_min,
        max=operator_max,

    ),
    'BBN_int_socket': OrderedDict(
        add=operator.add,
        subtract=operator.sub,
        multiply=operator.mul,
        division=operator.truediv,
        floor_division=operator.truediv,
        power=operator.pow,
        min=operator_min,
        max=operator_max,
    ),
    'BBN_vector_socket': OrderedDict(
        angle=mathutils.Vector.angle,
        cross=mathutils.Vector.cross,
        dot=mathutils.Vector.dot,
        lerp=mathutils.Vector.lerp,
        negate=mathutils.Vector.negate,
        normalize=mathutils.Vector.normalize,
        orthogonal=mathutils.Vector.orthogonal,
        project=mathutils.Vector.project,
        reflect=mathutils.Vector.reflect,
        rotate=mathutils.Vector.rotate,
        slerp=mathutils.Vector.slerp,
        length=mathutils.Vector.length,
        length_squared=mathutils.Vector.length_squared,
        magnitude=mathutils.Vector.magnitude,
        x=mathutils.Vector.x,
        y=mathutils.Vector.y,
        z=mathutils.Vector.z,
        xyz=mathutils.Vector.xyz,
        multiply=operator.mul,
        multiply_float=operator.mul,
        multiply_matrix=operator.matmul,
        add=operator.add,
        add_float=lambda a, b: operator.add(a, mathutils.Vector((b, b, b))),
        subtract=operator.sub,
        subtract_float=lambda a, b: operator.sub(a, mathutils.Vector((b, b, b))),
        divide_float=operator.truediv,

        # TODO: rotate by euler or quaterion

        # TODO: implement these aswell (Quaternions are now implemented)
        # to_track_quat = mathutils.Vector.to_track_quat,
        # rotation_difference = mathutils.Vector.rotation_difference,

    ),
    'BBN_matrix_socket': OrderedDict(
        adjugate=mathutils.Matrix.adjugate,
        decompose=mathutils.Matrix.decompose,
        determinant=mathutils.Matrix.determinant,
        multiply=operator.matmul,
        invert=mathutils.Matrix.invert,
        invert_safe=mathutils.Matrix.invert_safe,
        lerp=mathutils.Matrix.lerp,
        normalize=mathutils.Matrix.normalize,
        rotate_matrix=rotate_matrix,
        rotate_quaternion=rotate_matrix,
        rotate_euler=lambda a, b: rotate_matrix(a, mathutils.Euler(b)),
        to_euler=mathutils.Matrix.to_euler,
        to_quaternion=mathutils.Matrix.to_quaternion,
        to_scale=mathutils.Matrix.to_scale,
        to_translation=mathutils.Matrix.to_scale,
        transpose=mathutils.Matrix.transpose,
        is_negative=mathutils.Matrix.is_negative,
        is_orthogonal=mathutils.Matrix.is_orthogonal,
        is_orthogonal_axis_vectors=mathutils.Matrix.is_orthogonal_axis_vectors,
        median_scale=mathutils.Matrix.median_scale,
        translation=mathutils.Matrix.translation,
        column=lambda a, b: mathutils.Vector((a.col[b][0], a.col[b][1], a.col[b][2])),
    ),
    'BBN_quaternion_socket': OrderedDict(
        conjugate=mathutils.Quaternion.conjugate,
        cross=mathutils.Quaternion.cross,
        dot=mathutils.Quaternion.dot,
        identity=mathutils.Quaternion.identity,
        invert=mathutils.Quaternion.identity,
        make_compatible=mathutils.Quaternion.make_compatible,
        negate=mathutils.Quaternion.negate,
        normalize=mathutils.Quaternion.normalize,
        rotate=mathutils.Quaternion.rotate,
        rotation_difference=mathutils.Quaternion.rotation_difference,
        slerp=mathutils.Quaternion.slerp,
        to_axis_angle=mathutils.Quaternion.to_axis_angle,
        to_euler=mathutils.Quaternion.to_euler,
        to_exponential_map=mathutils.Quaternion.to_exponential_map,
        to_matrix=lambda a: mathutils.Quaternion.to_matrix(a).to_4x4(),
        to_swing_twist=mathutils.Quaternion.to_swing_twist,
        angle=mathutils.Quaternion.angle,
        axis=mathutils.Quaternion.axis,
        magnitude=mathutils.Quaternion.magnitude,
        w=mathutils.Quaternion.w,
        x=mathutils.Quaternion.x,
        y=mathutils.Quaternion.y,
        z=mathutils.Quaternion.z,
    ),
}

self_modifiing_functions = {
    'adjugate', 'invert', 'invert_safe', 'normalize', 'transpose',
    'negate', 'normalize', 'zero',
    'conjugate'
}

# TODO:
opt_behabiours = {
    'BBN_float_socket': {

    },
    'BBN_int_socket': {

    },
    'BBN_vector_socket': {

    },
    'BBN_matrix_socket': {

    },
    'BBN_quaternion_socket': {
        'to_euler': {
            'INPUTS': OrderedDict(
                Order={'type': 'BBN_enum_socket', 'items': ['XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX']},
                # Euler_Compatible = {'type': 'BBN_vector_socket'},
            )
        }
    },
}

behaviours = {
    'BBN_float_socket': {
        'add': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'subtract': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'multiply': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'division': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'floor_division': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'power': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'deg_to_rad': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'rad_to_deg': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'ceil': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_int_socket'},
            )
        },
        'absolute': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'floor': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_int_socket'},
            )
        },
        'modulo': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'truncate': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_int_socket'},
            )
        },
        'lerp': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
                Factor={'type': 'BBN_float_socket', 'default_value': 0.5},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'acos': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'asin': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'atan': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'atan2': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'cos': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'sin': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'tan': {
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'min': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
        'max': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Float={'type': 'BBN_float_socket'},
            )
        },
    },
    'BBN_int_socket': {
        'add': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'subtract': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'multiply': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'division': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'floor_division': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'power': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'min': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },
        'max': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Integer={'type': 'BBN_int_socket'},
            )
        },

    },
    'BBN_vector_socket': {
        'angle': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Angle={'type': 'BBN_float_socket'},
            )
        },
        'cross': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'dot': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Dot_Product={'type': 'BBN_float_socket'},
            )
        },
        'lerp': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
                Factor={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'negate': {
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'normalize': {
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'orthogonal': {
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'project': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'reflect': {
            'INPUTS': OrderedDict(
                Mirror={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'rotate': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_matrix_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'slerp': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
                Factor={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'length': {
            'OUTPUTS': OrderedDict(
                Length={'type': 'BBN_float_socket'},
            )
        },
        'length_squared': {
            'OUTPUTS': OrderedDict(
                Length={'type': 'BBN_float_socket'},
            )
        },
        'magnitude': {
            'OUTPUTS': OrderedDict(
                magnitude={'type': 'BBN_float_socket'},
            )
        },
        'x': {
            'OUTPUTS': OrderedDict(
                X={'type': 'BBN_float_socket'},
            )
        },
        'y': {
            'OUTPUTS': OrderedDict(
                Y={'type': 'BBN_float_socket'},
            )
        },
        'z': {
            'OUTPUTS': OrderedDict(
                Z={'type': 'BBN_float_socket'},
            )
        },
        'xyz': {
            'OUTPUTS': OrderedDict(
                X={'type': 'BBN_float_socket'},
                Y={'type': 'BBN_float_socket'},
                Z={'type': 'BBN_float_socket'},
            )
        },
        'multiply': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'multiply_float': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'multiply_matrix': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_matrix_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },

        'add': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'add_float': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'subtract': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'subtract_float': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'divide_float': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'to_track_quat': {
            'INPUTS': OrderedDict(
                Track={'type': 'BBN_enum_socket', 'items': ['X', 'Y', 'Z', '-X', '-Y', '-Z']},
                Up={'type': 'BBN_enum_socket', 'items': ['X', 'Y', 'Z']},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'rotation_difference': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },

    },
    'BBN_matrix_socket': {
        'adjugate': {
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'decompose': {
            'OUTPUTS': OrderedDict(
                Translation={'type': 'BBN_vector_socket'},
                Quaternion={'type': 'BBN_quaternion_socket'},
                Scale={'type': 'BBN_vector_socket'},
            )
        },
        'multiply': {
            'INPUTS': OrderedDict(
                Matrix_B={'type': 'BBN_matrix_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'determinant': {
            'OUTPUTS': OrderedDict(
                Determinant={'type': 'BBN_float_socket'},
            )
        },
        'invert': {
            'OUTPUTS': OrderedDict(
                Invert={'type': 'BBN_matrix_socket'},
            )
        },
        'invert_safe': {
            'OUTPUTS': OrderedDict(
                Invert={'type': 'BBN_matrix_socket'},
            )
        },
        'lerp': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_matrix_socket'},
                Factor={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'normalize': {
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'rotate_matrix': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_matrix_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'rotate_quaternion': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_quaternion_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'rotate_euler': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_vector_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'to_euler': {
            'OUTPUTS': OrderedDict(
                Euler={'type': 'BBN_vector_socket'},
            )
        },
        'to_quaternion': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'to_scale': {
            'OUTPUTS': OrderedDict(
                Scale={'type': 'BBN_vector_socket'},
            )
        },
        'to_translation': {
            'OUTPUTS': OrderedDict(
                Translation={'type': 'BBN_vector_socket'},
            )
        },
        'transpose': {
            'OUTPUTS': OrderedDict(
                Transposed={'type': 'BBN_matrix_socket'},
            )
        },
        'is_negative': {
            'OUTPUTS': OrderedDict(
                Is_Negative={'type': 'BBN_bool_socket'},
            )
        },
        'is_orthogonal': {
            'OUTPUTS': OrderedDict(
                Is_Orthogonal={'type': 'BBN_bool_socket'},
            )
        },
        'is_orthogonal_axis_vectors': {
            'OUTPUTS': OrderedDict(
                Is_Orthogonal={'type': 'BBN_bool_socket'},
            )
        },
        'median_scale': {
            'OUTPUTS': OrderedDict(
                Average_Scale={'type': 'BBN_float_socket'},
            )
        },
        'translation': {
            'OUTPUTS': OrderedDict(
                Translation={'type': 'BBN_vector_socket'},
            )
        },
        'column': {
            'INPUTS': OrderedDict(
                Column={'type': 'BBN_int_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Column={'type': 'BBN_vector_socket'},
            )
        },
    },
    'BBN_quaternion_socket': {
        'conjugate': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'cross': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_quaternion_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'dot': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_quaternion_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'identity': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'invert': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'make_compatible': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'negate': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'normalize': {
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'rotate': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_quaternion_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'rotation_difference': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_quaternion_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'slerp': {
            'INPUTS': OrderedDict(
                Other={'type': 'BBN_quaternion_socket'},
                Factor={'type': 'BBN_float_socket'},
            ),
            'OUTPUTS': OrderedDict(
                Quaternion={'type': 'BBN_quaternion_socket'},
            )
        },
        'to_axis_angle': {
            'OUTPUTS': OrderedDict(
                Axis={'type': 'BBN_vector_socket'},
                Angle={'type': 'BBN_float_socket'},
            )
        },
        'to_euler': {
            'OUTPUTS': OrderedDict(
                Euler={'type': 'BBN_vector_socket'},
            )
        },
        'to_exponential_map': {
            'OUTPUTS': OrderedDict(
                Vector={'type': 'BBN_vector_socket'},
            )
        },
        'to_matrix': {
            'OUTPUTS': OrderedDict(
                Matrix={'type': 'BBN_matrix_socket'},
            )
        },
        'to_swing_twist': {
            'INPUTS': OrderedDict(
                Axis={'type': 'BBN_enum_socket', 'items': ['X', 'Y', 'Z']},
            ),
            'OUTPUTS': OrderedDict(
                Swing={'type': 'BBN_quaternion_socket'},
                Twist={'type': 'BBN_float_socket'},
            )
        },
        'angle': {
            'OUTPUTS': OrderedDict(
                Angle={'type': 'BBN_float_socket'},
            )
        },
        'axis': {
            'OUTPUTS': OrderedDict(
                Axis={'type': 'BBN_vector_socket'},
            )
        },
        'magnitude': {
            'OUTPUTS': OrderedDict(
                Magnitude={'type': 'BBN_float_socket'},
            )
        },
        'w': {
            'OUTPUTS': OrderedDict(
                W={'type': 'BBN_float_socket'},
            )
        },
        'x': {
            'OUTPUTS': OrderedDict(
                X={'type': 'BBN_float_socket'},
            )
        },
        'y': {
            'OUTPUTS': OrderedDict(
                Y={'type': 'BBN_float_socket'},
            )
        },
        'z': {
            'OUTPUTS': OrderedDict(
                Z={'type': 'BBN_float_socket'},
            )
        },
    },
}

functions_enum = {
    key: sorted([
        (x, x.replace('_', ' ').title(), x.replace('_', ' ').title(), 'NONE', i) for i, x in enumerate(item.keys())
    ], key=lambda a: a[0]) for key, item in math_functions.items()
}


class BBN_node_math(Node, BBN_node):
    bl_idname = 'bbn_node_math'
    bl_label = "Math"
    bl_icon = 'FORWARD'
    bl_width_default = 200

    def updated_socket(self, context):
        for i in reversed(range(0, len(self.inputs))):
            self.inputs.remove(self.inputs[i])

        self.create_sockets()

        self.current_behaviour = self.behaviour_enum[0][0]

        # self.update_behaviour_sockets()

    socket_type: bpy.props.EnumProperty(
        items=[
            ('BBN_float_socket', 'Float', 'float'),
            ('BBN_int_socket', 'Int', 'Int'),
            ('BBN_vector_socket', 'Vector', 'Vector'),
            ('BBN_matrix_socket', 'Matrix', 'Matrix'),
            ('BBN_quaternion_socket', 'Quaternion', 'Quaternion'),
        ],
        update=updated_socket
    )

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row2.prop(self, 'socket_type', text='')

    @ property
    def input_sockets(self):
        return {socket_names[self.socket_type]: {'type': self.socket_type}}

    @ property
    def behaviour_enum(self):
        return functions_enum[self.socket_type]

    @ property
    def behaviour_sockets(self):
        return behaviours[self.socket_type]

    def draw_label(self):
        return f"{socket_names[self.socket_type]}: {self.current_behaviour.replace('_', ' ').title()}"

    def process(self, context, id, path):
        args = [self.get_input_value(i) for i in range(0, len(self.inputs))]

        func = math_functions[self.socket_type][self.current_behaviour]

        if callable(func):
            result = func(*args)
        else:
            result = getattr(args[0], self.current_behaviour)
        # if the function does not return anything, but just changes the vector...
        if self.current_behaviour in self_modifiing_functions:
            result = args[0]

        if len(self.outputs) > 1:
            for i, x in enumerate(result):
                self.outputs[i].set_value(x)
        else:
            if result is None:
                raise ValueError('The provided arguments do not meet the requirements')
            self.outputs[0].set_value(result)
