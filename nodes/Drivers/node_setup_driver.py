import bpy
from bpy.types import Node
from ..node_base import BBN_node

from ...node_tree import BBN_tree
from ...runtime import runtime_info

driver_types = [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.Driver.bl_rna.properties['type'].enum_items[:])]


def updated_type(self, context):
    try:
        expression_socket = next(x for x in self.inputs if x.name == 'Expression')
    except StopIteration:
        expression_socket = None
    try:
        use_self_socket = next(x for x in self.inputs if x.name == 'Use Self')
    except StopIteration:
        use_self_socket = None

    if self.driver_type == 'SCRIPTED':
        if not expression_socket:
            expression_socket = self.inputs.new('BBN_string_socket', 'Expression')._init()
            self.inputs.move(len(self.inputs) - 1, 0)
        if not use_self_socket:
            use_self_socket = self.inputs.new('BBN_bool_socket', 'Use Self')._init()
            self.inputs.move(len(self.inputs) - 1, 1)
    else:
        if expression_socket:
            self.inputs.remove(expression_socket)
        if use_self_socket:
            self.inputs.remove(use_self_socket)

    BBN_tree.value_updated(self, context)


class BBN_node_setup_driver(Node, BBN_node):
    bl_idname = 'bbn_setup_driver'
    bl_label = "Setup Driver"
    bl_icon = 'DRIVER'

    driver_type: bpy.props.EnumProperty(items=driver_types, update=updated_type)

    input_sockets = {
        'Add Driver Variable': {'type': 'BBN_add_array_socket'},
    }

    output_sockets = {
        'Driver': {'type': 'BBN_driver_socket'},
    }

    executable = False

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)
        layout.prop(self, 'driver_type', text='')
        layout.operator('bbn.add_driver_var_node', text='', icon='ADD').node = self.name

    def process(self, context, id, path):

        ans = {
            'driver_type': self.driver_type,
            'expression': self.get_input_value('Expression', default=''),
            'use_self': self.get_input_value('Use Self', default=False),
            'variables': [self.get_input_value(i) for i, x in enumerate(self.inputs) if x.bl_rna.name == 'BBN_drivervar_socket'],
        }

        self.outputs[0].set_value(ans)

    def update(self):
        if not self.can_update():
            return
        self.update_others()

        min_sockets = 3 if self.driver_type == 'SCRIPTED' else 1

        input = self.inputs['Add Driver Variable']
        connected_socket = input.connected_socket
        if connected_socket:
            link = input.links[0]
            if connected_socket.bl_rna.name == 'BBN_drivervar_socket':
                in_socket = self.inputs.new(connected_socket.bl_rna.name, connected_socket.bl_label)._init()
                self.id_data.relink_socket(input, in_socket)

        self.remove_incorrect_links()

        if len(self.inputs) > min_sockets:
            for i in reversed(range(min_sockets, len(self.inputs))):
                if not self.inputs[i].links and self.inputs[i].bl_rna.name == 'BBN_drivervar_socket':
                    self.inputs.remove(self.inputs[i])
