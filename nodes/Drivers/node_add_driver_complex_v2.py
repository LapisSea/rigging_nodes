import bpy
from bpy.types import Node

from ..node_base import BBN_node

behaviour_sockets_dict = {
    'SINGLE': {
        'SINGLE_POSE_BONE': {
            'INPUTS': {
                'Armature': {'type': 'BBN_object_socket'},
                'datapath': {'type': 'BBN_bone_socket', 'display_name': 'Bone', 'override_index': 1},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
        'SINGLE_BONE': {
            'INPUTS': {
                'Armature': {'type': 'BBN_object_socket'},
                'datapath': {'type': 'BBN_bone_socket', 'display_name': 'Bone', 'override_index': 1},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
        'SINGLE': {
            'INPUTS': {
                'Armature': {'type': 'BBN_object_socket', 'display_name': 'Object'},
                'datapath': {'type': 'BBN_string_socket', 'display_name': 'Datapath', 'override_index': 1},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
        'SINGLE_POINTER': {
            'INPUTS': {
                'Armature': {'type': 'BBN_pointer_socket', 'display_name': 'Pointer'},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
    },
    'MULTIPLE': {
        'MULTIPLE_POSE_BONES': {
            'INPUTS': {
                'Armature': {'type': 'BBN_object_socket'},
                'datapath': {'type': 'BBN_string_array_v2_socket', 'display_name': 'Bones', 'override_index': 1},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
        'MULTIPLE_BONES': {
            'INPUTS': {
                'Armature': {'type': 'BBN_object_socket'},
                'datapath': {'type': 'BBN_string_array_v2_socket', 'display_name': 'Bones', 'override_index': 1},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Armature': {'type': 'BBN_object_socket', 'display_name': 'Object'},
                'datapath': {'type': 'BBN_string_array_v2_socket', 'display_name': 'Datapaths', 'override_index': 1},
                'Property': {'type': 'BBN_string_socket'},
                'Index': {'type': 'BBN_int_socket', 'default_value': -1},
                'Driver': {'type': 'BBN_driver_socket'},
            },
        },
    }
}

behaviour_enum_dict = {
    'SINGLE': [
        ('SINGLE_POSE_BONE', 'Pose Bone', 'Only affect one bone', '', 0),
        ('SINGLE_BONE', 'Bone', 'Only affect one bone', '', 1),
        ('SINGLE', 'Datapath', 'Only affect one datapath', '', 2),
        ('SINGLE_POINTER', 'Pointer', 'Only affect one datapath', '', 3),
    ],
    'MULTIPLE': [
        ('MULTIPLE_POSE_BONES', 'Pose Bones', 'Add driver to multiple pose bones', '', 10),
        ('MULTIPLE_BONES', 'Bones', 'Add driver to multiple bones', '', 11),
        ('MULTIPLE', 'Datapaths', 'Add driver to multiple datapaths', '', 12),
        # ('MULTIPLE_POINTERS', 'Pointers', 'Add driver to multiple pointers', '', 13),
    ],

}

# ('MULTIPLE_POINTERS', 'Multiple Pointers', 'Add driver to multiple datapaths', '', 7),


class BBN_node_add_driver_complex_v2(Node, BBN_node):
    bl_idname = 'bbn_add_driver_complex_v2'
    bl_label = "Add Driver Advanced"
    bl_icon = 'DRIVER'

    bl_width_default = 250

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    def updated_mode(self, context):
        self.current_behaviour = self.behaviour_enum[0][0]

        # self.update_behaviour_sockets()

    mode: bpy.props.EnumProperty(
        items=[
            ('SINGLE', 'Single', 'Single'),
            ('MULTIPLE', 'Multiple', 'Multiple'),
        ],
        update=updated_mode
    )

    @ property
    def behaviour_enum(self):
        return behaviour_enum_dict[self.mode]

    @ property
    def behaviour_sockets(self):
        return behaviour_sockets_dict[self.mode]

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'mode', text='')

    def process(self, context, id, path):
        object = self.get_input_value("Armature", required=True)
        datapaths = self.get_input_value("datapath", default=None)

        if self.current_behaviour == 'SINGLE_POINTER':
            object, datapaths, obj_type = object

        self.focus_on_object(object, {'POSE'}, context)

        if not datapaths:
            raise ValueError('Datapath value is not valid')

        if self.current_behaviour in {'SINGLE_POSE_BONE', 'SINGLE', 'SINGLE_BONE', 'SINGLE_POINTER'}:
            datapaths = [datapaths]

        if self.current_behaviour in {'SINGLE_POSE_BONE', 'MULTIPLE_POSE_BONES'}:
            datapaths = [f'pose.bones["{x}"]' for x in datapaths]
        elif self.current_behaviour in {'SINGLE_BONE', 'MULTIPLE_BONES'}:
            datapaths = [f'data.bones["{x}"]' for x in datapaths]

        prop = self.get_input_value("Property")
        if not prop:
            raise ValueError('Property name is not valid')
        index = self.get_input_value('Index')
        driver_data = self.get_input_value("Driver")
        if not driver_data:
            raise ValueError('Driver data not valid')

        if not datapaths:
            datas = [object]
        else:
            datas = [object.path_resolve(datapath) for datapath in datapaths]

        for data in datas:
            if type(data) in {bpy.types.PoseBone, bpy.types.Bone}:
                object.data.BBN_info.add_bone_tracking(data.name, self, path)
            if isinstance(data, bpy.types.Constraint):
                bone_path = data.path_from_id().split('.constraints')[0]
                bone = object.path_resolve(bone_path)
                object.data.BBN_info.add_bone_tracking(bone.name, self, path)
                object.data.BBN_info.add_constraint_tracking(bone.name, data.name, self, path)
            try:
                drivers = data.driver_add(prop, index)
            except TypeError as e:
                if 'not found' in e.args[0]:
                    try:
                        drivers = data.driver_add(f'["{prop}"]', index)
                    except TypeError as e2:
                        raise e2 from e
                else:
                    raise

            if type(drivers) is not list:
                drivers = [drivers]

            for driver in drivers:
                d = driver.driver
                d.type = driver_data['driver_type']
                d.use_self = driver_data['use_self']
                d.expression = driver_data["expression"]

                for var in driver_data['variables']:
                    v = d.variables.new()
                    v.name = var['variable_name']
                    v.type = var['variable_type']

                    v.targets[0].id = var['target0_object']
                    v.targets[0].data_path = var['target0_datapath']
                    v.targets[0].bone_target = var['target0_bone']

                    v.targets[0].transform_type = var['target0_transform_type']
                    v.targets[0].rotation_mode = var['target0_rotation_mode']
                    v.targets[0].transform_space = var['target0_transform_space']

                    if var['variable_type'] == 'LOC_DIFF' or var['variable_type'] == 'ROTATION_DIFF':
                        v.targets[1].id = var['target1_object']
                        v.targets[1].data_path = var['target1_datapath']
                        v.targets[1].bone_target = var['target1_bone']

                        v.targets[1].transform_type = var['target1_transform_type']
                        v.targets[1].rotation_mode = var['target1_rotation_mode']
                        v.targets[1].transform_space = var['target1_transform_space']

        self.outputs['Object'].set_value(object)

        #! TODO: investigate why not setting it back to object mode can crash blender
        # it may be related to having a hidden object selected?
        #self.focus_on_object(armature, {'OBJECT'})
