import bpy
from bpy.types import Node
from ..node_base import BBN_node

from ...node_tree import BBN_tree


class BBN_node_vector(Node, BBN_node):
    bl_idname = 'bbn_vector_node'
    bl_label = "Const Vector"
    bl_icon = 'IPO_CONSTANT'

    input_sockets = {
        'X': {'type': 'BBN_float_socket'},
        'Y': {'type': 'BBN_float_socket'},
        'Z': {'type': 'BBN_float_socket'},
    }

    output_sockets = {
        'Value': {'type': 'BBN_vector_socket'},
    }

    def draw_label(self):
        x = self.get_input_value('X', default=0)
        y = self.get_input_value('X', default=0)
        z = self.get_input_value('X', default=0)
        return f'Vector: [{x:.2f},{y:.2f},{z:.2f}]'

    def process(self, context, id, path):
        val = [self.get_input_value(0), self.get_input_value(1), self.get_input_value(2)]
        self.outputs['Value'].set_value(val)
