from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..input_suboject_base import BBN_nodebase_input_subobject
from ...node_tree import BBN_tree


class BBN_node_input_armature(Node, BBN_nodebase_input_subobject):
    bl_idname = 'bbn_input_armature_node'
    bl_label = "Input Armature"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    stored_datablock: bpy.props.PointerProperty(type=bpy.types.Armature)

    def create_datablock(self, context):
        self.stored_datablock = bpy.data.armatures.new(self.name)

        self.edit_datablock(context)

        bpy.ops.armature.bone_primitive_add()
