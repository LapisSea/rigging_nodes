from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..input_suboject_base import BBN_nodebase_input_subobject
from ...node_tree import BBN_tree


class BBN_node_input_curve(Node, BBN_nodebase_input_subobject):
    bl_idname = 'bbn_input_curve_node'
    bl_label = "Input Curve"
    bl_icon = 'OUTLINER_OB_CURVE'

    stored_datablock: bpy.props.PointerProperty(type=bpy.types.Curve)

    def create_datablock(self, context):
        self.stored_datablock = bpy.data.curves.new(self.name, 'CURVE')

        self.edit_datablock(context)
