import bpy
import mathutils
import operator
from bpy.types import Node

from ..node_base import BBN_node

operations = [
    ('==', '==', 'Equals'),
    ('<', '<', 'Less than'),
    ('<=', '<=', 'Less than or equal'),
    ('>', '>', 'Greater than'),
    ('>=', '>=', 'Greater than or equal'),
    ('!=', '!=', 'Not equal'),
]

operation_functions = {
    '==': operator.eq,
    '<': operator.lt,
    '<=': operator.le,
    '>': operator.gt,
    '>=': operator.ge,
    '!=': operator.ne,
}


class BBN_node_get_vertex_positions(Node, BBN_node):
    bl_idname = 'bbn_get_vertex_positions_node'
    bl_label = "Get vertex Positions"
    bl_width_default = 300

    input_sockets = {
        'Mesh': {'type': 'BBN_object_ref_socket'},
        'Group Name': {'type': 'BBN_string_socket', 'default_value': 'Group'},
        'Weight': {'type': 'BBN_float_socket', 'default_value': 1.0},
    }

    _opt_input_sockets = {
        'Weight Check': {
            'type': 'BBN_enum_socket',
            'items': operations
        },
    }

    _opt_output_sockets = {
        # 'Pointers': {'type': 'BBN_pointer_array_socket'},
        'Normals': {'type': 'BBN_vector_array_socket'},
        'Positions': {'type': 'BBN_vector_array_socket'},
        'Undeformed Positions': {'type': 'BBN_vector_array_socket'},
    }

    def process(self, context, id, path):
        obj = self.get_input_value('Mesh', required=True)
        group_name = self.get_input_value('Group Name')
        weight = self.get_input_value('Weight')
        op = operation_functions[self.get_input_value('Weight Check', default='==')]

        verts = [x for x in obj.data.vertices if any(obj.vertex_groups[y.group].name == group_name and op(y.weight, weight) for y in x.groups)]

        if self.has_output('Positions'):
            self.set_output_value('Positions', [x.co.copy() for x in verts])

        if self.has_output('Undeformed Positions'):
            self.set_output_value('Undeformed Positions', [x.undeformed_co.copy() for x in verts])

        if self.has_output('Normals'):
            self.set_output_value('Normals', [x.normal.copy() for x in verts])

        if self.has_output('Pointers'):
            self.set_output_value('Pointers', [(obj.data, x.path_from_id(), bpy.types.MeshVertex.bl_rna) for x in verts])
