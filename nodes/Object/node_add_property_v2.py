import bpy
from bpy.types import Node

from ..node_base import BBN_node
from collections import OrderedDict


class BBN_node_add_property_v2(Node, BBN_node):
    bl_idname = 'bbn_add_property_v2'
    bl_label = "Add Custom Property"
    bl_icon = 'PROPERTIES'

    bl_width_default = 200

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    @ property
    def opt_output_sockets(self):
        ans = {
            'Datapath': {'type': 'BBN_string_socket'},
            'As Driver': {'type': 'BBN_driver_socket'},
            'As Driver Var': {'type': 'BBN_drivervar_socket'},
        }
        if self.data_type in {'BONE', 'POSE_BONE'}:
            ans['Bone'] = {'type': 'BBN_string_socket'}
        return ans

    def _updated_behaviour(self, context):
        self.updated_behaviour(context)

    value_type: bpy.props.EnumProperty(
        items=[
            ('FLOAT', 'Float', 'Float', 'NONE', 0),
            ('INTEGER', 'Interger', 'Interger', 'NONE', 1),
            ('STRING', 'String', 'String', 'NONE', 2),
        ],
        update=_updated_behaviour
    )

    data_type: bpy.props.EnumProperty(
        items=[
            ('POSE_BONE', 'Pose Bone', 'Pose Bone', 'NONE', 0),
            ('BONE', 'Bone', 'Bone', 'NONE', 1),
            ('OBJECT', 'Object', 'Object', 'NONE', 2),
            ('DATA', 'Data', 'Data', 'NONE', 3),
        ],
        update=_updated_behaviour
    )

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'data_type', text='')
        row1.prop(self, 'value_type', text='')

    @property
    def current_behaviour_inputs(self):
        data = self.data_sockets[self.data_type]
        value = self.value_sockets[self.value_type]

        inputs = {**data['INPUTS'], **value['INPUTS']}
        return inputs

    data_sockets = {
        'OBJECT': {
            'INPUTS': {
                'Object': {'type': 'BBN_object_socket'},
                'Name': {'type': 'BBN_string_socket'},
            }
        },
        'DATA': {
            'INPUTS': {
                'Object': {'type': 'BBN_object_socket'},
                'Name': {'type': 'BBN_string_socket'},
            }
        },
        'POSE_BONE': {
            'INPUTS': {
                'Object': {'type': 'BBN_object_socket', 'display_name': 'Armature'},
                'Bone': {'type': 'BBN_bone_socket', 'override_index': 1},
                'Name': {'type': 'BBN_string_socket'},
            }
        },
        'BONE': {
            'INPUTS': {
                'Object': {'type': 'BBN_object_socket', 'display_name': 'Armature'},
                'Bone': {'type': 'BBN_bone_socket', 'override_index': 1},
                'Name': {'type': 'BBN_string_socket'},
            }
        }
    }

    value_sockets = {
        'FLOAT': {
            'INPUTS': {
                'Value': {'type': 'BBN_float_socket'},
                'Min': {'type': 'BBN_float_socket'},
                'Max': {'type': 'BBN_float_socket', 'default_value': 1.0},
            }
        },
        'INTEGER': {
            'INPUTS': {
                'Value': {'type': 'BBN_int_socket'},
                'Min': {'type': 'BBN_int_socket'},
                'Max': {'type': 'BBN_int_socket', 'default_value': 1},
            }
        },
        'STRING': {
            'INPUTS': {
                'Value': {'type': 'BBN_string_socket'},
            }
        }
    }

    def init(self, context):
        super().init(context)

    def process(self, context, id, path):
        object = self.get_input_value("Object", required=True)

        bone_name = self.get_input_value("Bone", default='')
        prop_name = self.get_input_value("Name")

        if not bone_name and self.data_type in {'BONE', 'POSE_BONE'}:
            raise ValueError('Bone Name is not valid')

        if not prop_name:
            raise ValueError('Property name is not valid')

        prop_value = self.get_input_value("Value")

        if self.data_type == 'POSE_BONE':
            self.focus_on_object(object, {'POSE'})
            data = object.pose.bones.get(bone_name)
            if not data:
                raise ValueError('Bone not found')
            object.data.BBN_info.add_bone_tracking(bone_name, self, path)
            data_path = f'pose.bones["{bone_name}"]["{prop_name}"]'
        elif self.data_type == 'BONE':
            self.focus_on_object(object, {'OBJECT'})
            data = object.data.bones.get(bone_name)
            if not data:
                raise ValueError('Bone not found')
            object.data.BBN_info.add_bone_tracking(bone_name, self, path)
            data_path = f'data.bones["{bone_name}"]["{prop_name}"]'
        elif self.data_type == 'OBJECT':
            data = object
            if not data:
                raise ValueError('Object is not valid')
            data_path = f'["{prop_name}"]'
        elif self.data_type == 'DATA':
            data = object.data
            if not data:
                raise ValueError('Object has no valid data')
            data_path = f'data["{prop_name}"]'

        data[prop_name] = prop_value
        if '_RNA_UI' not in data:
            data['_RNA_UI'] = {}

        if self.value_type in {'FLOAT', 'INTEGER'}:
            min = self.get_input_value("Min")
            max = self.get_input_value("Max")
            data['_RNA_UI'][prop_name] = {'min': min, 'soft_min': min, 'max': max, 'soft_max': max, 'description': '', 'default': prop_value}
        else:
            data['_RNA_UI'][prop_name] = {'description': '', 'default': prop_value}

        data.property_overridable_library_set(f'["{prop_name}"]', True)

        self.set_output_value('Object', object)
        self.set_output_value('Bone', bone_name)
        self.set_output_value('Datapath', data_path)

        if self.has_output('As Driver'):
            ans = {
                'driver_type': 'AVERAGE',
                'expression': '',
                'use_self': False,
                'variables': [
                    {
                        'variable_name': prop_name,
                        'variable_type': 'SINGLE_PROP',

                        'target0_object': object,
                        'target0_bone': bone_name,
                        'target0_datapath': data_path,
                        'target0_transform_type': 'LOC_X',
                        'target0_rotation_mode': 'AUTO',
                        'target0_transform_space': 'WORLD_SPACE',

                        'target1_object': None,
                        'target1_bone': '',
                        'target1_datapath': '',
                        'target1_transform_type': 'LOC_X',
                        'target1_rotation_mode': 'AUTO',
                        'target1_transform_space': 'WORLD_SPACE',
                    }
                ],
            }
            self.set_output_value('As Driver', ans)
        if self.has_output('As Driver Var'):
            ans = {
                'variable_name': prop_name,
                'variable_type': 'SINGLE_PROP',

                'target0_object': object,
                'target0_bone': bone_name,
                'target0_datapath': data_path,
                'target0_transform_type': 'LOC_X',
                'target0_rotation_mode': 'AUTO',
                'target0_transform_space': 'WORLD_SPACE',

                'target1_object': None,
                'target1_bone': '',
                'target1_datapath': '',
                'target1_transform_type': 'LOC_X',
                'target1_rotation_mode': 'AUTO',
                'target1_transform_space': 'WORLD_SPACE',
            }
            self.set_output_value('As Driver Var', ans)
