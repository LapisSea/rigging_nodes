from ...runtime import cache_loop_index, cache_loop_inputs, cache_node_group_outputs, runtime_info
import bpy
from ..node_base import BBN_node

from bpy.types import NodeCustomGroup, Node, Operator
from mathutils import Vector

import uuid


class BBN_OP_create_loop(Operator):
    bl_idname = "bbn.create_loop"
    bl_label = "Create Loop"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree
        orig_node = bone_tree.nodes[self.node]

        new_node_tree = bpy.data.node_groups.new("LOOP_new_loop", "bbn_tree_loop")
        orig_node.node_tree_selection = new_node_tree
        loop_info = new_node_tree.nodes.new("bbn_loop_info")
        loop_input = new_node_tree.nodes.new("NodeGroupInput")
        loop_output = new_node_tree.nodes.new("NodeGroupOutput")
        bone_set_property = new_node_tree.nodes.new("bbn_set_bone_property_node")
        get_node = new_node_tree.nodes.new("bbn_get_from_array_node")
        get_node.current_behaviour = 'BBN_string_array_v2_socket'

        loop_info.location = [-300, -40]
        loop_input.location = [-300, 70]
        loop_output.location = [300, 70]
        bone_set_property.location = [100, 120]
        get_node.location = [-100, 40]

        loop_info.select = False
        loop_input.select = False
        loop_output.select = False
        bone_set_property.select = True
        get_node.select = True

        new_node_tree.links.new(get_node.outputs[0], bone_set_property.inputs[1])
        new_node_tree.links.new(loop_info.outputs[0], get_node.inputs[1])

        new_node_tree.inputs.new('BBN_int_socket', 'Start Index')
        new_node_tree.inputs.new('BBN_int_socket', 'End Index')

        orig_node.node_tree_selection = new_node_tree
        return {'FINISHED'}


class BBN_OP_setup_loop(Operator):
    '''Creates "Start Index" and "End Index" sockets in the node group if they don't exist'''

    bl_idname = "bbn.setup_loop"
    bl_label = "Setup Loop"

    node_group: bpy.props.StringProperty()

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = bpy.data.node_groups[self.node_group]

        if 'Start Index' not in tree.inputs:
            tree.inputs.new('BBN_int_socket', 'Start Index')
        if 'End Index' not in tree.inputs:
            tree.inputs.new('BBN_int_socket', 'End Index')

        return {'FINISHED'}


class BBN_node_loop(NodeCustomGroup, BBN_node):
    '''Converts old loop trees into the new types'''
    bl_idname = "bbn_node_loop"
    bl_label = "Loop"
    bl_icon = 'FILE_REFRESH'
    bl_width_default = 250

    log_time = False

    dependent_classes = [BBN_OP_create_loop, BBN_OP_setup_loop]

    def loop_tree_filter(self, context):
        """Define which tree we would like to use as loop trees."""
        if context:
            if context.bl_idname != 'bbn_tree_loop':  # It should be our dedicated to this class
                return False
            else:
                # to avoid circular dependencies
                for path_tree in bpy.context.space_data.path:
                    if path_tree.node_tree.name == context.name:
                        return False
                return True
        return False

    def update_loop_tree(self, context):
        if self.node_tree != self.node_tree_selection:
            for i in reversed(range(len(self.inputs))):
                self.inputs.remove(self.inputs[i])
            for i in reversed(range(len(self.outputs))):
                self.outputs.remove(self.outputs[i])
            self.node_tree = self.node_tree_selection

    node_tree_selection: bpy.props.PointerProperty(type=bpy.types.NodeTree, poll=loop_tree_filter, update=update_loop_tree)

    def draw_label(self):
        if self.node_tree:
            return self.node_tree.name

        return 'Node Group'

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        col = layout.column(align=True)

        row = col.row(align=True)

        if self.node_tree and self.node_tree.library:
            op = row.operator('wm.open_mainfile', text='', icon='BLENDER')
            op.filepath = bpy.path.abspath(self.node_tree.library.filepath)
            op.display_file_selector = False

        row.template_ID(self, "node_tree_selection")

        if not self.node_tree:
            row.operator('bbn.create_group', text='', icon='ADD').node = self.name

    def pre_process(self, context, id, path):
        start_index = self.get_input_value('Start Index', required=True, throw=True)
        end_index = self.get_input_value('End Index', required=True, throw=True)

        if end_index - start_index <= 0:
            raise ValueError('The loop will not be executed')

        # Cache all the output nodes
        if self.node_tree not in cache_node_group_outputs:
            cache_node_group_outputs[self.node_tree] = []
            for x in self.node_tree.nodes:
                if x.bl_rna.identifier == 'NodeGroupOutput':
                    cache_node_group_outputs[self.node_tree].append(x)

        path = path + [self.name]

        cache_loop_inputs[self.node_tree] = {}

        #print(f'{" "*len(path)}LOOP {self.node_tree.name}')
        for i in range(start_index, end_index):
            cache_loop_index[self.node_tree] = i
            execute_id = str(uuid.uuid4())
            outputs = set()

            # first execute everything
            for x in cache_node_group_outputs[self.node_tree]:
                self.execute_other(context, execute_id, path, x)

            # after everything is executed gather the results for the next loop
            for x in cache_node_group_outputs[self.node_tree]:
                # if we are on the last loop, set the values of the output sockets
                if i == end_index - 1:
                    for socket in x.inputs:
                        if socket.name not in outputs and socket.links:
                            try:
                                output = next(y for y in self.outputs if y.name == socket.name)
                            except StopIteration:
                                continue

                            output.set_value(socket.get_value())
                            outputs.add(socket.name)

                        elif socket.links:
                            raise ValueError(f'Socket {x}:{socket.name} has already been set by another Group Output Node')
                        else:
                            pass
                else:
                    for socket in x.inputs:
                        if socket.bl_idname != 'NodeSocketVirtual':
                            if socket.name not in outputs and socket.links:
                                found_value = socket.get_value()
                                #print(f'{" "*(len(path)+1)}{i} :: {found_value}')
                                cache_loop_inputs.setdefault(self.node_tree, {})[socket.name] = found_value
                                outputs.add(socket.name)
                            elif socket.links:
                                raise ValueError(f'Socket {x}:{socket.name} has already been set by another Group Output Node')
                            else:
                                pass
                    pass
