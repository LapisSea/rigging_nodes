import bpy

from .node_base import BBN_node
from mathutils import Vector

props_dict = {}

# these properties are defined in python as utility functions
# in bpy_types.py in class _GenericBone
extra_properties = {
    bpy.types.Bone: {
        'x_axis': {'description': 'Axis', 'name': 'X axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
        'y_axis': {'description': 'Axis', 'name': 'Y axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
        'z_axis': {'description': 'Axis', 'name': 'Z axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
    },
    bpy.types.EditBone: {
        'x_axis': {'description': 'Axis', 'name': 'X axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
        'y_axis': {'description': 'Axis', 'name': 'Y axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
        'z_axis': {'description': 'Axis', 'name': 'Z axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
    },
    bpy.types.PoseBone: {
        'x_axis': {'description': 'Axis', 'name': 'X axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
        'y_axis': {'description': 'Axis', 'name': 'Y axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
        'z_axis': {'description': 'Axis', 'name': 'Z axis', 'type': 'FLOAT', 'is_array': True, 'array_length': 3, 'default_array': Vector((0.0, 0.0, 0.0)), 'subtype': 'XYZ'},
    }
}


class BBN_node_get_property(BBN_node):
    bl_idname = 'bbn_get_property_node'
    bl_label = "Get Property"
    bl_icon = 'BONE_DATA'

    bl_width_default = 300

    rna_props_to_get = {'name', 'type', 'subtype', 'enum_items', 'default', 'fixed_type', 'icon', 'is_array', 'array_dimensions', 'array_length', 'default_array', 'is_runtime', 'description'}
    props_to_get = {}

    behaviour_enum = {
        ('SINGLE', 'Single', 'Single'  'NONE', 0),
        ('MULTIPLE', 'Multiple', 'Multiple', 'NONE', 2),
    }

    as_string = False

    def draw_label(self):
        if len(self.outputs) == len(self.output_sockets.keys()) + 1:
            return f'Get {self.outputs[(len(self.outputs) - 1)].name}'

        return self.bl_label

    @property
    def props_class(self):
        return bpy.types.PoseBone

    def get_props(self, cls):
        if not cls:
            return {}
        if cls in props_dict:
            return props_dict[cls]

        if self.props_to_get:
            props = (x for x in cls.bl_rna.properties if x.identifier in self.props_to_get)
        else:
            props = cls.bl_rna.properties

        ans = {x.identifier: {y.identifier: getattr(x, y.identifier) for y in x.bl_rna.properties if y.identifier in self.rna_props_to_get} for x in props}

        if 'space_subtarget' in ans:
            ans['space_subtarget']['name'] = 'Sub-Target Space'

        ans.update(extra_properties.get(self.props_class, {}))

        props_dict[cls] = ans
        return ans

    def get_current_props(self):
        return self.get_props(self.props_class)

    def create_socket_dict(self):
        props = self.get_current_props()

        sockets_dict = {}

        for prop_name, prop_info in props.items():

            if prop_name == 'space_subtarget':
                prop_info['name'] = 'Sub-Target Space'
            if not self.as_string:
                if self._current_behaviour == 'SINGLE':
                    if prop_info['type'] == 'STRING':
                        sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket'}
                    elif prop_info['type'] == 'ENUM':
                        sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_enum_socket', 'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(prop_info['enum_items'])]}
                    elif prop_info['type'] == 'BOOLEAN':
                        if prop_info['is_array'] is False:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_socket'}
                        elif prop_info['array_length'] == 3:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_array_socket', 'required_length': 3}
                    elif prop_info['type'] == 'POINTER':
                        if prop_info['fixed_type'].bl_rna.identifier == 'Object':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Action':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_action_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Text':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_text_socket'}
                        else:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_pointer_socket'}
                    elif prop_info['type'] == 'FLOAT':
                        if not prop_info['is_array']:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_socket'}
                        else:
                            if prop_info['array_length'] == 3:
                                sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_vector_socket', 'default_value': prop_info['default_array']}
                            elif prop_info['subtype'] == 'MATRIX':
                                if prop_info['array_length'] == 16:
                                    sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_matrix_socket', 'default_value': prop_info['default_array']}
                            elif prop_info['array_dimensions'][0] == prop_info['array_length']:
                                sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_array_socket', 'required_length': prop_info['array_length'], 'default_value': prop_info['default_array']}
                            else:
                                pass
                                # print(prop_info)
                    elif prop_info['type'] == 'INT':
                        sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_socket'}
                    elif prop_info['type'] == 'COLLECTION':
                        if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                        else:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_collection_socket'}
                    else:
                        pass
                elif self._current_behaviour == 'MULTIPLE':
                    if prop_info['type'] == 'STRING':
                        sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    if prop_info['type'] == 'ENUM':
                        sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['type'] == 'BOOLEAN':
                        if prop_info['is_array'] is False:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_array_socket'}
                    elif prop_info['type'] == 'POINTER':
                        if prop_info['fixed_type'].bl_rna.identifier == 'Object':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_array_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Action':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_action_array_socket'}
                        elif prop_info['fixed_type'].bl_rna.identifier == 'Text':
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_text_array_socket'}
                        else:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_pointer_array_socket'}
                    elif prop_info['type'] == 'FLOAT':
                        if not prop_info['is_array']:
                            sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_array_socket'}
                        else:
                            if prop_info['array_length'] == 3:
                                sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_vector_array_socket'}
                            elif prop_info['subtype'] == 'MATRIX' and prop_info['array_length'] == 16:
                                sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_matrix_array_socket'}
                    # print(f'\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n{prop_name}:{prop_info}\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n')
            else:
                if self._current_behaviour == 'SINGLE':
                    sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket'}
                elif self._current_behaviour == 'MULTIPLE':
                    sockets_dict[prop_name] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}

            if prop_name not in sockets_dict:
                pass
                # print(f'NOT CREATED {self.props_class} : {prop_info}')
        for key in sockets_dict.keys():
            sockets_dict[key]['is_deletable'] = True
        return sockets_dict

    @ property
    def opt_output_sockets(self):
        return self.create_socket_dict()

    def get_attribute(self, prop_info, id, obj, prop_name):
        if prop_info['type'] == 'POINTER' and prop_info['fixed_type'].bl_rna.identifier in {'EditBone', 'PoseBone', 'Bone'}:
            pointer = getattr(obj, prop_name)
            if pointer:
                return pointer.name
            else:
                return ''

        elif prop_info['type'] == 'POINTER' and prop_info['fixed_type'].bl_rna.identifier == 'Object':
            return getattr(obj, prop_name)

        elif prop_info['type'] == 'COLLECTION' and prop_info['fixed_type'].bl_rna.identifier in {'EditBone', 'PoseBone', 'Bone'}:
            attr = getattr(obj, prop_name)
            return ([x.name for x in attr])

        elif prop_info['type'] == 'COLLECTION' or prop_info['type'] == 'POINTER':
            try:
                attr = getattr(obj, prop_name)
            except AttributeError:
                return None
            try:
                path = obj.path_from_id()
                path += f'.{prop_name}'
            except ValueError:
                path = attr.path_from_id()

            if id != attr.id_data:
                path = f'data.{path}'
            return (id, path, prop_info['fixed_type'])
        else:
            ans = getattr(obj, prop_name)
            return ans

    def set_output(self, id, socket, obj, prop_name):
        prop_info = self.get_current_props()[prop_name]

        if self._current_behaviour == 'SINGLE':
            self.set_output_value(socket, self.get_attribute(prop_info, id, obj, prop_name))
        elif self._current_behaviour == 'MULTIPLE':
            ans = []
            for i in range(0, len(obj)):
                ans.append(self.get_attribute(prop_info, id[i], obj[i], prop_name))
            self.set_output_value(socket, ans)

    def set_outputs(self, id, obj):
        for socket in self.outputs:
            if socket.name in self.opt_output_sockets.keys():
                self.set_output(id, socket, obj, socket.name)
            else:
                print(f'"{socket.name}" socket info not found')
