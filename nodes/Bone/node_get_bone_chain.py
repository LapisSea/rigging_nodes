import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_get_bone_chain(Node, BBN_node):
    bl_idname = 'bbn_node_get_bone_chain'
    bl_label = "Get Bone Chain"
    bl_icon = 'BONE_DATA'

    bl_width_default = 250

    input_sockets = {
        'Armature': {'type': 'BBN_object_ref_socket'},
        'Start': {'type': 'BBN_bone_socket'},
        'End': {'type': 'BBN_bone_socket'},
    }

    output_sockets = {
        'Array': {'type': 'BBN_string_array_v2_socket'},
    }

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)

        end_bone = armature.data.bones[self.get_input_value("End")]
        start_bone = armature.data.bones[self.get_input_value("Start")]
        current_bone = end_bone

        bone_list = []
        list_correct = False

        while current_bone:
            bone_list.append(current_bone.name)
            if current_bone == start_bone:
                current_bone = None
                list_correct = True
            else:
                current_bone = current_bone.parent

        if not list_correct:
            raise ValueError('The bones do not form a chain')

        bone_list.reverse()

        self.outputs['Array'].set_value(bone_list)
