from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_collection_add(Node, BBN_node):
    bl_idname = 'bbn_collection_add_node'
    bl_label = "Edit Collection"

    input_sockets = {
        'Collection': {'type': 'BBN_collection_socket'},
    }
    output_sockets = {
        'Collection': {'type': 'BBN_collection_socket'},
    }

    behaviour_enum = [
        ('APPEND', 'Append', 'Append'),
        ('GET', 'Get', 'Get'),
        ('REMOVE', 'Remove', 'Remove'),
        ('APPEND_MULTIPLE', 'Append Multiple', 'Append multiple'),
    ]

    behaviour_sockets = {
        'APPEND': {
            'OUTPUTS': {
                'Item': {'type': 'BBN_pointer_socket'}
            }
        },
        'APPEND_MULTIPLE': {
            'INPUTS': {
                'Number': {'type': 'BBN_int_socket'}
            },
            'OUTPUTS': {
                'Items': {'type': 'BBN_pointer_array_socket'}
            }
        },
        'GET': {
            'INPUTS': {
                'Index': {'type': 'BBN_int_socket'}
            },
            'OUTPUTS': {
                'Item': {'type': 'BBN_pointer_socket'}
            }
        },
        'REMOVE': {
            'INPUTS': {
                'Index': {'type': 'BBN_int_socket'}
            }
        },
    }

    node_labels = {
        'APPEND': 'Add to collection',
        'GET': 'Get item',
        'REMOVE': 'Remove from collection',
        'APPEND_MULTIPLE': 'Add multiple items',
    }

    def draw_label(self):
        return self.node_labels[self.current_behaviour]

    def process(self, context, id, path):
        source_object, datapath, rna_type = self.get_input_value(0)

        collection = source_object.path_resolve(datapath)

        if collection is None:
            raise ValueError(f'{source_object} -> {datapath} cannot be resolved')

        if self.current_behaviour == 'APPEND':
            try:
                new_item = collection.add()
            except AttributeError:
                new_item = collection.new()
            new_prop_datapath = datapath + '[' + str(len(collection) - 1) + ']'

            self.outputs['Item'].set_value((source_object, new_prop_datapath, rna_type))

        elif self.current_behaviour == 'APPEND_MULTIPLE':
            ans = []
            for i in range(self.get_input_value('Number')):
                try:
                    new_item = collection.add()
                except AttributeError:
                    new_item = collection.new()
                new_prop_datapath = datapath + '[' + str(len(collection) - 1) + ']'
                ans.append((source_object, new_prop_datapath, rna_type))

            self.outputs['Items'].set_value(ans)

        elif self.current_behaviour == 'GET':
            index = self.get_input_value('Index')
            if index >= len(collection):
                raise ValueError('The given index is invalid')
            item = collection[index]
            new_prop_datapath = datapath + '[' + str(index) + ']'

            self.outputs['Item'].set_value((source_object, new_prop_datapath, rna_type))

        elif self.current_behaviour == 'REMOVE':
            index = self.get_input_value('Index')
            if index >= len(collection):
                raise ValueError('The given index is invalid')
            collection.remove(index)

        self.outputs['Collection'].set_value((source_object, datapath, rna_type))
