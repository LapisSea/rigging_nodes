import bpy
from ..node_base import BBN_node

from bpy.types import Node


class BBN_node_group_in(Node, BBN_node):
    deprecated = True

    bl_idname = "bbn_group_in_node"
    bl_label = "Group Input"
    bl_icon = 'GROUP'

    node_version = 1

    def upgrade_node(self):
        super().upgrade_node()
        for x in self.outputs:
            x.editable_name = True
            x.is_moveable = True
            x.is_deletable = True

        self.current_node_version = self.node_version
