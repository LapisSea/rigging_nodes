from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_angle_between_vectors(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_angle_between_vectors_node'
    bl_label = "Angle"
    bl_icon = 'DRIVER_ROTATIONAL_DIFFERENCE'

    input_sockets = {
        'V0': {'type': 'BBN_vector_socket'},
        'V1': {'type': 'BBN_vector_socket'},
    }

    output_sockets = {
        'Angle': {'type': 'BBN_float_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, replace it with the math node')
