import bpy
import warnings
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_add_driver_complex(Node, BBN_node):
    bl_idname = 'bbn_add_driver_complex'
    bl_label = "Add Driver Advanced"
    bl_icon = 'DRIVER'

    deprecated = True

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},
        'Datapath': {'type': 'BBN_string_socket'},
        'Property': {'type': 'BBN_string_socket'},
        'Index': {'type': 'BBN_int_socket', 'default_value': -1},
        'Driver': {'type': 'BBN_driver_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Add driver to multiple datapaths', '', 1),
        # ('SINGLE_BONE', 'Single Bone', 'Only affect one bone', '', 2),
        # ('MULTIPLE_BONES', 'Multiple Bones', 'Add driver to multiple datapaths', '', 3),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Datapath': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Datapath': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    input_to_focus = 'Object'
    focus_mode = {'POSE'}

    def init(self, context):
        super().init(context)

    def process(self, context, id, path):
        armature = self.get_input_value("Object", required=True)

        datapaths = self.get_input_value("Datapath")
        if self.current_behaviour == 'SINGLE':
            if datapaths:
                datapaths = [datapaths]

        prop = self.get_input_value("Property")
        index = self.get_input_value('Index')
        driver_data = self.get_input_value("Driver")

        if not datapaths:
            datas = [armature]
        else:
            datas = [armature.path_resolve(datapath) for datapath in datapaths]

        for data in datas:
            drivers = data.driver_add(prop, index)
            if type(drivers) is not list:
                drivers = [drivers]

            for driver in drivers:
                d = driver.driver
                d.type = driver_data['driver_type']
                d.use_self = driver_data['use_self']
                d.expression = driver_data["expression"]

                for var in driver_data['variables']:
                    v = d.variables.new()
                    v.name = var['variable_name']
                    v.type = var['variable_type']

                    v.targets[0].id = var['target0_object']
                    v.targets[0].data_path = var['target0_datapath']
                    v.targets[0].bone_target = var['target0_bone']

                    v.targets[0].transform_type = var['target0_transform_type']
                    v.targets[0].rotation_mode = var['target0_rotation_mode']
                    v.targets[0].transform_space = var['target0_transform_space']

                    if var['variable_type'] == 'LOC_DIFF' or var['variable_type'] == 'ROTATION_DIFF':
                        v.targets[1].id = var['target1_object']
                        v.targets[1].data_path = var['target1_datapath']
                        v.targets[1].bone_target = var['target1_bone']

                        v.targets[1].transform_type = var['target1_transform_type']
                        v.targets[1].rotation_mode = var['target1_rotation_mode']
                        v.targets[1].transform_space = var['target1_transform_space']

        self.outputs['Object'].set_value(armature)

        #! TODO: investigate why not setting it back to object mode can crash blender
        # it may be related to having a hidden object selected?
        #self.focus_on_object(armature, {'OBJECT'})
        warnings.warn('"Add driver complex" has received an update, replace it with the new one to get all the new features', DeprecationWarning)
