import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_not(Node, BBN_node):
    deprecated = True
    bl_idname = 'bbn_not_node'
    bl_label = "Not"

    input_sockets = {
        'Val': {'type': 'BBN_bool_socket'},
    }

    output_sockets = {
        'Result': {'type': 'BBN_bool_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('Replace with the "logic" node')
