import bpy

from bpy.types import Node
from ..node_base import BBN_node


class BBN_node_break_vector(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_break_vector_node'
    bl_label = "Break Vector"
    bl_icon = 'PRESET'

    input_sockets = {
        'Vector': {'type': 'BBN_vector_socket'},
    }

    output_sockets = {
        'X': {'type': 'BBN_float_socket'},
        'Y': {'type': 'BBN_float_socket'},
        'Z': {'type': 'BBN_float_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('Replace with the math node (Vector -> xyz)')
