import bpy
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info


class BBN_node_to_string(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_to_string_node'
    bl_label = "To String"
    bl_icon = 'SYNTAX_OFF'

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    output_sockets = {
        'String': {'type': 'BBN_string_socket'},
    }

    def init(self, context):
        super().init(context)

    def update(self):

        if not self.can_update():
            return
        self.update_others()

        main_input = self.inputs[0]
        connected_socket = main_input.connected_socket
        if main_input.bl_rna.name == 'BBN_add_array_socket' and connected_socket:
            self.array_socket_type = connected_socket.bl_rna.name

            new_socket = self.inputs.new(self.array_socket_type, 'Item')._init()

            self.id_data.relink_socket(main_input, new_socket)

            self.inputs.remove(main_input)

        elif not main_input.links:
            self.array_socket_type = ''
            self.inputs.remove(main_input)
            new_socket = self.inputs.new('BBN_add_array_socket', 'Item')._init()

        self.remove_incorrect_links()

    def process(self, context, id, path):
        raise ValueError('This node is not necessary, connecting a socket to a string socket will convert it to string automatically')
