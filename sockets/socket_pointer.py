import bpy
from bpy.types import NodeSocket, NodeSocketInterface
from ..node_tree import BBN_tree
from ..runtime import cache_socket_variables
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_pointer_base(BBN_socketmixin):
    color = (0.2, 0.8, 0.2, 1)


class BBN_socket_pointer_interface(BBN_pointer_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_pointer_socket'


class BBN_socket_pointer(BBN_pointer_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_pointer_socket'
    bl_label = "Pointer"

    source_object: bpy.props.PointerProperty(type=bpy.types.Object)
    data_path: bpy.props.StringProperty()

    show_extra_info = False

    def convert_value(self, value):
        '''sets the value already serialized'''
        if value is None:
            return None
        else:
            return (value[0], value[1], value[2])

    def get_real_value(self):
        val = self.get_value()
        if val:
            source_object, datapath, rna_type = val
            try:
                return source_object.path_resolve(datapath)
            except ValueError:
                return None
        return None

    def value_to_string(self, value):
        if value:
            obj, datapath, rna_type = value

            if obj:
                try:
                    return f'{rna_type.name}'
                except ReferenceError:
                    return f'(REMOVED)'
        return '(NONE)'


class BBN_socket_pointer_array_interface(BBN_array_base, BBN_pointer_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_pointer_array_socket'


class BBN_socket_pointer_array(BBN_array_base, BBN_pointer_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_pointer_array_socket'
    bl_label = "Pointer Array"

    socket_class = BBN_socket_pointer
