import bpy
from bpy.types import NodeSocket, NodeSocketInterface
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_driver_base(BBN_socketmixin):
    color = (0.6, 0.3, 1.0, 1.0)


class BBN_socket_driver_interface(BBN_driver_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_driver_socket'


class BBN_socket_driver(BBN_driver_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_driver_socket'
    bl_label = "Driver"

    def value_to_string(self, value):
        if value:
            return value['driver_type']
        return '(NONE)'

    def draw_buttons(self, context, layout):
        if not self.is_output and not self.is_linked:
            op = layout.operator('bbn.add_driver_node', text='', icon='ADD')
            op.node = self.node.name
            op.socket_index = next(i for i, x in enumerate(self.node.inputs) if x == self)


class BBN_socket_driver_array_interface(BBN_array_base, BBN_driver_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_driver_array_socket'


class BBN_socket_driver_array(BBN_array_base, BBN_driver_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_driver_array_socket'
    bl_label = "Driver Array"

    socket_class = BBN_socket_driver
