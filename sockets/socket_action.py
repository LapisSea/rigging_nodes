import bpy
from bpy.types import NodeSocket, NodeSocketInterface
from ..node_tree import BBN_tree
from .socket import BBN_socket, BBN_socket_array, BBN_array_base, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin


class BBN_action_base(BBN_socketmixin):
    color = (1.0, 0.3, 0.5, 1)
    shape = 'DIAMOND_DOT'


class BBN_socket_action_interface(BBN_action_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_action_socket'


class BBN_socket_action(BBN_action_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_action_socket'
    bl_label = "Action"

    default_value: bpy.props.PointerProperty(type=bpy.types.Action, update=BBN_tree.value_updated)

    icon = 'ACTION'

    def value_to_string(self, value):
        if value:
            try:
                return f'{value.name}'
            except ReferenceError:
                return f'(REMOVED)'
        return '(NONE)'


class BBN_socket_action_array_interface(BBN_array_base, BBN_action_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_action_array_socket'


class BBN_socket_action_array(BBN_array_base, BBN_action_base, BBN_socket_array, NodeSocket):
    bl_idname = 'BBN_action_array_socket'
    bl_label = "Action Array"

    socket_class = BBN_socket_action
