import bpy

tree_usage_info = {}


class BBN_OP_inspect_tree(bpy.types.Operator):
    bl_idname = "bbn.inspect_tree"
    bl_label = "Inspect Tree"

    node: bpy.props.StringProperty()

    bl_options = {'INTERNAL'}

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        trees_to_inspect = [(context.space_data.node_tree, [context.space_data.node_tree.name])]
        inspected_trees = set()
        usage_data = tree_usage_info[context.space_data.node_tree] = {}

        while trees_to_inspect:
            tree, parent_node_path = trees_to_inspect.pop(0)
            inspected_trees.add(tree)

            if tree != context.space_data.node_tree:
                usage_data.setdefault(tree, []).append(parent_node_path)

            for node in tree.nodes:
                if hasattr(node, 'node_tree'):
                    sub_tree = node.node_tree

                    if sub_tree:
                        # if the tree was already inspected, continue
                        if sub_tree not in inspected_trees:
                            path = node.create_path_to_self(parent_node_path)
                            trees_to_inspect.append((sub_tree, path))

        return {'FINISHED'}


class BBN_PT_tree_usage_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_tree_usage_panel"
    bl_label = "Group Usage Information"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Rigging Nodes"

    @classmethod
    def poll(cls, context):
        # return True
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        layout = self.layout
        layout.operator('bbn.inspect_tree')
        tree_usage = tree_usage_info.get(context.space_data.node_tree, {})

        if tree_usage:
            for tree, paths in sorted(tree_usage.items(), key=lambda a: a[0].name):
                box = layout.box()
                box.label(text=tree.name)
                row = box.row()
                row.separator(factor=1)
                column = row.column()
                for path in paths:
                    column.operator('bbn.focus_path_node', text='->'.join(path)).path = '$$$$'.join(path)


classes = [
    BBN_OP_inspect_tree,
    BBN_PT_tree_usage_panel,
]


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
