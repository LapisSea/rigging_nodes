from bpy.types import Operator
import bpy
import mathutils


class BBN_OP_auto_frame_delete(Operator):
    bl_idname = "bbn.auto_frame_delete"
    bl_label = "Auto Frame Delete"

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        all_nodes = [x for x in tree.nodes if x.bl_rna.identifier == 'NodeFrame' and '__auto_generated__' in x]
        for node in all_nodes:
            tree.nodes.remove(node)

        return {'FINISHED'}
