import bpy
from bpy.types import Operator
import mathutils


class BBN_OP_edit_bone_shape(Operator):
    '''Moves the custom shape object to the active pose bone shape location and add is to the scene for easier bone shape editing.
The created object can then be deleted and this operator can be later called again to edit the shape at any time'''
    bl_idname = "bbn.edit_bone_shape"
    bl_label = "Edit Bone Shape"

    @classmethod
    def poll(cls, context):
        return context.active_pose_bone and context.active_pose_bone.custom_shape

    def execute(self, context):
        bone = context.active_pose_bone
        edit_obj = bone.custom_shape

        tm_bone = bone if not bone.custom_shape_transform else bone.custom_shape_transform
        tm = tm_bone.matrix.copy()
        bone_scale = bone.length * bone.custom_shape_scale if bone.use_custom_shape_bone_size else bone.custom_shape_scale
        bone_scale /= tm.median_scale

        if edit_obj.name not in context.scene.collection.objects:
            context.scene.collection.objects.link(edit_obj)
        bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='DESELECT')
        scale_tm_x = mathutils.Matrix.Scale(bone_scale, 4, mathutils.Vector((1, 0, 0)))
        scale_tm_y = mathutils.Matrix.Scale(bone_scale, 4, mathutils.Vector((0, 1, 0)))
        scale_tm_z = mathutils.Matrix.Scale(bone_scale, 4, mathutils.Vector((0, 0, 1)))
        final_tm = tm @ scale_tm_x @ scale_tm_y @ scale_tm_z
        edit_obj.matrix_world = final_tm
        edit_obj.select_set(True)
        context.view_layer.objects.active = edit_obj

        if edit_obj.library:
            edit_obj.make_local()
        if edit_obj.data.library:
            edit_obj.data.make_local()
        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.select_mode(type='VERT')
            bpy.ops.mesh.select_all(action='SELECT')
        else:
            self.report({'INFO'}, 'The custom object cannot be edited')
        return {'FINISHED'}
