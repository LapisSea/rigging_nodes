from bpy.types import Operator
import bpy


class BBN_OP_new_datablock(Operator):
    '''Create a new datablock for this node'''
    bl_idname = "bbn.new_datablock"
    bl_label = "New Datablock"

    node: bpy.props.StringProperty()

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)
        node.create_datablock(context)

        return {'FINISHED'}
