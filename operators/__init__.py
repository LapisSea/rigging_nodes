import bpy
import os
import importlib


tree = [x[:-3] for x in os.listdir(os.path.dirname(__file__)) if x.endswith('.py') and x != '__init__.py']

for i in tree:
    importlib.import_module('.' + i, package=__package__)

__globals = globals().copy()

operators = set()

for num_id, x in enumerate([x for x in __globals if x.startswith('op_')]):
    for y in [item for item in dir(__globals[x]) if item.startswith('BBN_OP_')]:
        operators.add(getattr(__globals[x], y))

operators = list(operators)


def register():
    from bpy.utils import register_class

    dependent_classes = []

    for cls in operators:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    dependent_classes = set()

    for cls in reversed(operators):
        unregister_class(cls)
