import bpy
from bpy.types import Operator


class BBN_OP_remove_created_object(Operator):
    '''Unlinks the object from the nodetree so it will not be replaced when re-executing the tree'''
    bl_idname = "bbn.remove_created_object"
    bl_label = "Unlink created object"

    index: bpy.props.IntProperty()

    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree' and context.space_data.node_tree

    def execute(self, context):
        tree = context.space_data.node_tree
        if tree:
            tree.unlink_object(self.index)
        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_confirm(self, event)
