import bpy
from ..runtime import runtime_info


class BBN_OP_update_nodes(bpy.types.Operator):
    '''Updates old nodes (useful when updating the addon)\nBACKUP YOUR WORK FIRST!'''

    bl_idname = "bbn.update_nodes"
    bl_label = "Update Nodes"

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        runtime_info['updating'] = True

        for bone_tree in bpy.data.node_groups:
            if bone_tree.rna_type.identifier in {'bbn_tree', 'bbn_tree_group', 'bbn_tree_loop'}:
                for node in bone_tree.nodes:
                    if hasattr(node, 'current_node_version'):
                        if node.current_node_version != node.node_version:
                            node.upgrade_node()
                            self.report({'INFO'}, f'Updated node: "{node.name}"')
        return {'FINISHED'}
