import bpy


class BBN_OP_select_referencing_nodes(bpy.types.Operator):
    '''Selects the "Get" nodes that reference this variable'''
    bl_idname = "bbn.select_referencing_nodes"
    bl_label = "Select Referencing Nodes"

    bl_options = {'INTERNAL'}

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        nodes = [x for x in tree.nodes if x.bl_idname == 'bbn_get_variable' and x.variable_name == node.variable_name]

        if nodes:
            bpy.ops.node.select_all(action='DESELECT')
            tree.nodes.active = nodes[0]
            for x in nodes:
                x.select = True
            bpy.ops.node.view_selected()

        return {'FINISHED'}
