from bpy.types import Operator
import mathutils
import bpy


class BBN_OP_add_driver_var_node(Operator):
    bl_idname = "bbn.add_driver_var_node"
    bl_label = "Add Driver Var Node"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        new_node = tree.nodes.new("bbn_add_driver_variable")

        num_sockets = len(node.inputs) - next(i for i, x in enumerate(node.inputs) if x.name == 'Add Driver Variable') - 1

        new_node.location = node.location + mathutils.Vector((-300, -100 + (-150 * num_sockets)))

        tree.links.new(new_node.outputs[0], next(x for x in node.inputs if x.name == 'Add Driver Variable'))

        return {'FINISHED'}
