from bpy.types import Operator
import bpy


class BBN_OP_select_register_object(Operator):
    '''Selects the registered object in the 3d view'''
    bl_idname = "bbn.select_register_object"
    bl_label = "Select Object"

    bl_options = {'INTERNAL'}

    index: bpy.props.IntProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.node_tree
        register_data = tree.objects[self.index]

        bpy.ops.object.select_all(action='DESELECT')

        obj = register_data.value

        if obj:
            try:
                obj.select_set(True)
            except RuntimeError:
                self.report({'ERROR'}, 'The object is not in the current view layer')
                return {'CANCELLED'}

        return {'FINISHED'}
